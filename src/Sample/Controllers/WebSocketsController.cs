﻿using System;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace Sample.Controllers
{
    [Route("websockets")]
    public class WebsocketsController
        : ControllerBase
    {
        [HttpPost]
        public IActionResult AddConnection([FromBody] MyPayload myPayload)
        {
            Console.WriteLine("IT GETS TO THE CONTROLLER'S ACTION!");
            Console.WriteLine($"MyPayload: {myPayload}");
            var isWorking = myPayload?.Message != null;
            if (!isWorking)
            {
                Console.WriteLine($"IT DOES NOT DESERIALIZE!!");
                using var reader = new StreamReader(Request.Body, Encoding.UTF8);
                var payload = reader.ReadToEnd();
                Console.WriteLine($"BUT REQUEST PAYLOAD IS: {payload}");
            }
            return Ok(myPayload);
        }
    }
}