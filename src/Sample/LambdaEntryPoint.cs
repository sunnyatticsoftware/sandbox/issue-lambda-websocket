using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.AspNetCoreServer.Internal;
using Amazon.Lambda.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Hosting;

namespace Sample
{
    /// <summary>
    /// This class extends from APIGatewayProxyFunction which contains the method FunctionHandlerAsync which is the 
    /// actual Lambda function entry point. The Lambda handler field should be set to
    /// 
    /// Sample::Sample.LambdaEntryPoint::FunctionHandlerAsync
    /// </summary>
    public class LambdaEntryPoint :

        // The base class must be set to match the AWS service invoking the Lambda function. If not Amazon.Lambda.AspNetCoreServer
        // will fail to convert the incoming request correctly into a valid ASP.NET Core request.
        //
        // API Gateway REST API                         -> Amazon.Lambda.AspNetCoreServer.APIGatewayProxyFunction
        // API Gateway HTTP API payload version 1.0     -> Amazon.Lambda.AspNetCoreServer.APIGatewayProxyFunction
        // API Gateway HTTP API payload version 2.0     -> Amazon.Lambda.AspNetCoreServer.APIGatewayHttpApiV2ProxyFunction
        // Application Load Balancer                    -> Amazon.Lambda.AspNetCoreServer.ApplicationLoadBalancerFunction
        // 
        // Note: When using the AWS::Serverless::Function resource with an event type of "HttpApi" then payload version 2.0
        // will be the default and you must make Amazon.Lambda.AspNetCoreServer.APIGatewayHttpApiV2ProxyFunction the base class.

        Amazon.Lambda.AspNetCoreServer.APIGatewayProxyFunction
    {
        /// <summary>
        /// The builder has configuration, logging and Amazon API Gateway already configured. The startup class
        /// needs to be configured in this method using the UseStartup<>() method.
        /// </summary>
        /// <param name="builder"></param>
        protected override void Init(IWebHostBuilder builder)
        {
            builder
                .UseStartup<Startup>();
        }

        /// <summary>
        /// Use this override to customize the services registered with the IHostBuilder. 
        /// 
        /// It is recommended not to call ConfigureWebHostDefaults to configure the IWebHostBuilder inside this method.
        /// Instead customize the IWebHostBuilder in the Init(IWebHostBuilder) overload.
        /// </summary>
        /// <param name="builder"></param>
        protected override void Init(IHostBuilder builder)
        {
        }
        
        /// <summary>
        /// To access APIGatewayProxyRequest: HttpContext.Items["LambdaRequestObject"] as APIGatewayProxyRequest;
        /// To access ILambdaContext: HttpContext.Items["LambdaContext"] as ILambdaContext;
        /// </summary>
        /// <param name="features"></param>
        /// <param name="apiGatewayRequest"></param>
        /// <param name="lambdaContext"></param>
        protected override void MarshallRequest(
            InvokeFeatures features, 
            APIGatewayProxyRequest apiGatewayRequest, 
            ILambdaContext lambdaContext)
        {
            var isWebSocketRequest = apiGatewayRequest.HttpMethod is null;
            if (isWebSocketRequest)
            {
                apiGatewayRequest.Path = "/";
                apiGatewayRequest.Resource = "/";
                apiGatewayRequest.HttpMethod = "GET";
            
                lambdaContext.Logger.LogLine("Websocket request: " + JsonSerializer.Serialize(apiGatewayRequest));
                lambdaContext.Logger.LogLine("Lambda context: " + JsonSerializer.Serialize(lambdaContext));
                lambdaContext.Logger.LogLine($"Event Type: {apiGatewayRequest.RequestContext.EventType}");
                lambdaContext.Logger.LogLine($"Route Key: {apiGatewayRequest.RequestContext.RouteKey}");

                if (apiGatewayRequest.QueryStringParameters != null
                    && apiGatewayRequest.QueryStringParameters.ContainsKey("token"))
                {
                    var token = apiGatewayRequest.QueryStringParameters["token"];
                    apiGatewayRequest.Headers.Add("Authorization", $"Bearer {token}");
                    apiGatewayRequest.MultiValueHeaders.Add("Authorization", new List<string> { $"Bearer {token}" });
                }
            }
            base.MarshallRequest(features, apiGatewayRequest, lambdaContext);
        }

        /// <summary>
        /// Method called after marshalling incoming request
        /// </summary>
        /// <param name="aspNetCoreRequestFeature"></param>
        /// <param name="apiGatewayProxyRequest"></param>
        /// <param name="lambdaContext"></param>
        protected override void PostMarshallRequestFeature(
            IHttpRequestFeature aspNetCoreRequestFeature, 
            APIGatewayProxyRequest apiGatewayProxyRequest,
            ILambdaContext lambdaContext)
        {
            var isWebSocketRequest = apiGatewayProxyRequest.RequestContext.EventType != null;
            if (isWebSocketRequest)
            {
                var eventType = apiGatewayProxyRequest.RequestContext.EventType;
                // TODO depending on eventType CONNECT, DISCONNECT or MESSAGE do one thing or another
            
                var myPayload = new MyPayload {Message = "this is my new message"};
                var bodyJson = JsonSerializer.Serialize(myPayload);
                var bodyJsonBytes = Encoding.UTF8.GetBytes(bodyJson);
                var memoryStream = new MemoryStream(bodyJsonBytes);
                aspNetCoreRequestFeature.Path = $"/websockets";
                aspNetCoreRequestFeature.Method = "POST";
                aspNetCoreRequestFeature.Headers.Add("Content-Type", "application/json");
                aspNetCoreRequestFeature.Headers.Add("Accepts", "application/json");
                aspNetCoreRequestFeature.Body = memoryStream;
            }
        }
    }
}
