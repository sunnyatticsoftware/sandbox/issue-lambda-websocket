docker rm -fv $(docker ps -aq)
docker system prune --all -f
dotnet build -c Release
dotnet publish -c Release
docker build --no-cache -t issue/lambda-websocket:latest .
docker run -p 9000:8080 issue/lambda-websocket:latest "Sample::Sample.LambdaEntryPoint::FunctionHandlerAsync"