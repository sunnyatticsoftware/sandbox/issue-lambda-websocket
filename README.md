# issue-lambda-websocket

https://github.com/aws/aws-lambda-dotnet/issues/819

## To test with AWS Runtime Interface Emulator
```
dotnet build -c Release
dotnet publish -c Release
docker build --no-cache -t issue/lambda-websocket:latest .
docker run -p 9000:8080 issue/lambda-websocket:latest "Sample::Sample.LambdaEntryPoint::FunctionHandlerAsync"
```
Then send a POST request to http://localhost:9000/2015-03-31/functions/function/invocations
with the following payload sample
```
{
    "headers": {
        "Host": "82a1kbz4gh.execute-api.eu-west-3.amazonaws.com",
        "Sec-WebSocket-Extensions": "permessage-deflate; client_max_window_bits",
        "Sec-WebSocket-Key": "ElxPGSkjS67JEzN6/32V1A==",
        "Sec-WebSocket-Version": "13",
        "X-Amzn-Trace-Id": "Root=1-60351922-7cd582b44021ba371d4b975b",
        "X-Forwarded-For": "185.153.165.121",
        "X-Forwarded-Port": "443",
        "X-Forwarded-Proto": "https"
    },
    "multiValueHeaders": {
        "Host": [
            "82a1kbz4gh.execute-api.eu-west-3.amazonaws.com"
        ],
        "Sec-WebSocket-Extensions": [
            "permessage-deflate; client_max_window_bits"
        ],
        "Sec-WebSocket-Key": [
            "ElxPGSkjS67JEzN6/32V1A=="
        ],
        "Sec-WebSocket-Version": [
            "13"
        ],
        "X-Amzn-Trace-Id": [
            "Root=1-60351922-7cd582b44021ba371d4b975b"
        ],
        "X-Forwarded-For": [
            "185.153.165.121"
        ],
        "X-Forwarded-Port": [
            "443"
        ],
        "X-Forwarded-Proto": [
            "https"
        ]
    },
    "requestContext": {
        "routeKey": "$connect",
        "disconnectStatusCode": null,
        "messageId": null,
        "eventType": "CONNECT",
        "extendedRequestId": "bNDdbEgZCGYFjXQ=",
        "requestTime": "23/Feb/2021:15:02:58 +0000",
        "messageDirection": "IN",
        "disconnectReason": null,
        "stage": "production",
        "connectedAt": 1614092578600,
        "requestTimeEpoch": 1614092578601,
        "identity": {
            "cognitoIdentityPoolId": null,
            "cognitoIdentityId": null,
            "principalOrgId": null,
            "cognitoAuthenticationType": null,
            "userArn": null,
            "userAgent": null,
            "accountId": null,
            "caller": null,
            "sourceIp": "185.153.165.121",
            "accessKey": null,
            "cognitoAuthenticationProvider": null,
            "user": null
        },
        "requestId": "bNDdbEgZCGYFjXQ=",
        "domainName": "82a1kbz4gh.execute-api.eu-west-3.amazonaws.com",
        "connectionId": "bNDdbfUjCGYCG1Q=",
        "apiId": "82a1kbz4gh"
    },
    "isBase64Encoded": false
}
```


